/**
*
* 	AnsCDN Copyright (C) 2010 Robin Syihab (r [at] nosql.asia)
*	Simple CDN server written in Golang.
*
*	License: General Public License v2 (GPLv2)
*
*	Copyright (c) 2009 The Go Authors. All rights reserved.
*
**/

package main

import (
	"io/ioutil"
	"mime"
	"net/http"
	"os"
	"path"
	"strings"
)

func download(urlSource string, absPath string, strict bool, data *[]byte) (rv bool, lm string, totalSize int) {

	resp, err := http.Get(urlSource)
	if err != nil {
		logError("Cannot download data from `%s`. e: %s\n", urlSource, err.Error())
		return false, "", 0
	}

	*data, err = ioutil.ReadAll(resp.Body)

	if err != nil {
		logError("Cannot read url source body `%s`. error: %s\n", absPath, err.Error())
		return false, "", 0
	}

	// check for the mime
	contentType := resp.Header.Get("Content-Type")
	if endi := strings.IndexAny(contentType, ";"); endi > 1 {
		contentType = contentType[0:endi]
	} else {
		contentType = contentType[0:]
	}

	// fmt.Printf("Content-type: %s\n",ctype)
	if extType := mime.TypeByExtension(path.Ext(absPath)); extType != "" {
		if endi := strings.IndexAny(extType, ";"); endi > 1 {
			extType = extType[0:endi]
		} else {
			extType = extType[0:]
		}
		contentType := fixedMime(contentType)
		exttype := fixedMime(extType)
		if exttype != contentType {
			logWarn("Mime type different by extension. `%s` <> `%s` path `%s`\n", contentType, exttype, urlSource)
			if strict {
				return false, "", 0
			}
		}
	}

	logInfo("File `%s` first cached from `%s`.\n", absPath, urlSource)

	file, err := os.OpenFile(absPath, os.O_WRONLY|os.O_CREATE, 0755)
	if err != nil {
		logError("Cannot create file `%s`. error: %s\n", absPath, err.Error())
		return false, "", 0
	}
	defer file.Close()

	totalSize = len(*data)
	for {
		bw, err := file.Write(*data)
		if err != nil {
			logError("Cannot write %d bytes data in file `%s`. error: %s\n", totalSize, absPath, err.Error())
			return false, "", 0
		}
		if bw >= totalSize {
			break
		}
	}

	lm, _ = modTimeString(file)

	return true, lm, totalSize
}
