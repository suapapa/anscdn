/**
*
* 	AnsCDN Copyright (C) 2010 Robin Syihab (r [at] nosql.asia)
*	Simple CDN server written in Golang.
*
*	License: General Public License v2 (GPLv2)
*
*	Copyright (c) 2009 The Go Authors. All rights reserved.
*
**/

package main

import "log"

var logQuiet bool

func logInfo(format string, v ...interface{}) {
	if logQuiet {
		return
	}
	log.Printf("[info] "+format, v...)
}
func logWarn(format string, v ...interface{})  { log.Printf("[warning] "+format, v...) }
func logError(format string, v ...interface{}) { log.Printf("[error] "+format, v...) }
