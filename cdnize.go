/**
*
* 	AnsCDN Copyright (C) 2010 Robin Syihab (r [at] nosql.asia)
*	Simple CDN server written in Golang.
*
*	License: General Public License v2 (GPLv2)
*
*	Copyright (c) 2009 The Go Authors. All rights reserved.
*
**/

package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"net/http"
	"os"
	"path"
	"syscall"
)

func jsonError(status, info string) string {
	return jsonize(map[string]string{"Status": status, "Info": info})
}

func cdnHandler(c http.ResponseWriter, r *http.Request) {

	c.Header().Set("Content-Type", "application/json")

	apiKey := r.FormValue("api_key")
	//base_url := r.FormValue("base_url")

	if apiKey != cfg.APIKey {
		fmt.Fprintf(c, jsonError("failed", "Invalid api key"))
		return
	}

	reqURL := r.FormValue("u")
	if reqURL == "" {

		r.ParseForm()
		fileName := r.FormValue("file_name")

		if fileName == "" {
			fmt.Fprintf(c, jsonError("failed", "no `fileName` parameter"))
			return
		}

		fmt.Printf("fileName: %v\n", fileName)

		file, err := r.MultipartReader()
		if err != nil {
			fmt.Fprintf(c, jsonError("failed", "cannot get multipart reader"))
			return
		}

		part, err := file.NextPart()
		if err != nil {
			fmt.Fprintf(c, jsonError("failed", "no `u` nor `file`"))
			return
		}
		var data [1000]byte
		md5ed := md5.New()
		absPath := "/tmp/" + randString(100)
		dstFile, err := os.OpenFile(absPath, os.O_WRONLY|os.O_CREATE, 0755)
		if err != nil {
			logError("Cannot create file `%s`. error: %s\n", absPath, err.Error())
			fmt.Fprintf(c, jsonError("failed", fmt.Sprintf("cannot create temporary data. %v\n", err)))
			return
		}

		var dataSize int64
		for dataSize < r.ContentLength {
			i, err := part.Read(data[0:999])
			if err != nil {
				break
			}

			_, err = md5ed.Write(data[0:i])
			if err != nil {
				logError("Cannot calculate MD5 hash")
				fmt.Fprintf(c, jsonError("failed", "cannot calculate checksum"))
				break
			}

			_, err = dstFile.Write(data[0:i])
			if err != nil {
				logError("Cannot fmt.Fprintf %d bytes data in file `%s`. error: %s\n", dataSize, absPath, err.Error())
			}

			dataSize += int64(i)
		}

		dstFile.Close()

		//fmt.Printf("content-length: %v, file: %v, file-length: %v, i: %v\n", r.ContentLength, string(data[0:]), i, i)

		hash := hex.EncodeToString(md5ed.Sum(nil))
		fileExt := path.Ext(fileName)
		fileName = hash + randString(9) + fileExt
		newPath, err := os.Getwd()

		newPath = path.Join(newPath, cfg.StoreDir[2:], cfg.APIStorePrefix, fileName)

		if err != nil {
			logError("Cannot getwd\n")
			fmt.Fprintf(c, jsonError("failed", "internal error"))
			return
		}

		//fmt.Printf("absPath: %v, newPath: %v\n", abs_path, new_path)
		if err := syscall.Rename(absPath, newPath); err != nil {
			logError("Cannot move from file `%s` to `%s`. %v.\n", absPath, newPath, err)
			fmt.Fprintf(c, jsonError("failed", "internal error"))
			return
		}

		cdnizedURL := fmt.Sprintf("http://%s/%s/%s/%s", cfg.CdnServerName, cfg.StoreDir[2:], cfg.APIStorePrefix, fileName)

		logInfo("cdnizedURL: %s\n", cdnizedURL)

		os.Remove(absPath)

		type success struct {
			Status     string
			Size       int64
			CdnizedURL string
		}

		fmt.Fprintf(c, jsonize(&success{"ok", dataSize, cdnizedURL}))
		return
	}

	//fmt.Fprintf(c, fmt.Sprintf("{Status: 'ok', url_path: '%s', gen: '%s'}", reqURL, x))

	fileExt := path.Ext(reqURL)
	absPath, _ := os.Getwd()
	absPath = path.Join(absPath, cfg.StoreDir[2:], cfg.APIStorePrefix, randString(64)+fileExt)

	fmt.Printf("absPath: %s\n", absPath)

	var data []byte
	rv, lm, tsize := download(reqURL, absPath, true, &data)
	if rv != true {
		fmt.Fprintf(c, jsonError("failed", "Cannot fetch from source url"))
		return
	}

	md5ed := md5.New()
	for {
		brw, err := md5ed.Write(data)
		if err != nil {
			logError("Cannot calculate MD5 hash")
			fmt.Fprintf(c, jsonError("failed", "Internal error"))
			return
		}
		if brw >= tsize {
			break
		}
	}

	hash := hex.EncodeToString(md5ed.Sum(nil))
	dir, _ := path.Split(absPath)
	fileName := hash + randString(8) + fileExt
	newPath := path.Join(dir, fileName)

	if err := syscall.Rename(absPath, newPath); err != nil {
		logError("Cannot rename from file `%s` to `%s`", absPath, newPath)
		fmt.Fprintf(c, jsonError("failed", "Internal error"))
		return
	}

	cdnizedURL := fmt.Sprintf("http://%s/%s/%s/%s", cfg.CdnServerName, cfg.StoreDir[2:], cfg.APIStorePrefix, fileName)

	logInfo("cdnizedURL: %s", cdnizedURL)

	type success struct {
		Status     string
		Lm         string
		Size       int
		Original   string
		CdnizedURL string
	}

	fmt.Fprintf(c, jsonize(&success{"ok", lm, tsize, reqURL, cdnizedURL}))
}

func cdnStaticHandler(c http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	root, _ := os.Getwd()
	http.ServeFile(c, r, root+"/"+path)
}
