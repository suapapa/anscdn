/**
*
* 	AnsCDN Copyright (C) 2010 Robin Syihab (r [at] nosql.asia)
*	Simple CDN server written in Golang.
*
*	License: General Public License v2 (GPLv2)
*
*	Copyright (c) 2009 The Go Authors. All rights reserved.
*
**/

package main

import (
	"encoding/json"
	"errors"
	"os"
)

// AnscdnConf is struct for configuration
type AnscdnConf struct {
	BaseServer     string `json:"base_server"`
	ServingPort    int    `json:"serving_port"`
	StoreDir       string `json:"store_dir"`
	Strict         bool   `json:"strict"`
	CacheOnly      bool   `json:"cache_only"`
	FileMon        bool   `json:"file_mon"`
	CacheExpires   int64  `json:"cache_expires"`
	ClearCachePath string `json:"clear_cache_path"`
	IgnoreNoExt    bool   `json:"ignore_no_ext"`
	IgnoreExt      string `json:"ignore_ext"`
	ProvideAPI     bool   `json:"provide_api"`
	APIKey         string `json:"api_key"`
	CdnServerName  string `json:"cdn_server_name"`
	URLMap         string `json:"url_map"`
	APIStorePrefix string `json:"api_store_prefix"`
}

var defaultConfig = AnscdnConf{
	BaseServer:     "example.com",
	ServingPort:    2009,
	StoreDir:       "./data",
	Strict:         true,
	CacheOnly:      false,
	FileMon:        false,
	CacheExpires:   1296000,
	ClearCachePath: "/clearDataDir",
	IgnoreNoExt:    true,
	IgnoreExt:      "html,txt",
	ProvideAPI:     false,
	APIKey:         "12345",
	CdnServerName:  "static4.example.com",
	URLMap:         "/static",
	APIStorePrefix: "c",
}

func configParse(fileName string) (*AnscdnConf, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}

	cfg := &AnscdnConf{}
	dec := json.NewDecoder(file)
	err = dec.Decode(cfg)
	if err != nil {
		return nil, err
	}

	if len(cfg.BaseServer) == 0 {
		return nil, errors.New("No base server")
	}

	if cfg.ServingPort == 0 {
		return nil, errors.New("No port")
	}

	if len(cfg.StoreDir) == 0 {
		cfg.StoreDir = "./data"
	}

	if cfg.CacheExpires == 0 {
		cfg.CacheExpires = 1296000
	}

	return cfg, nil
}
