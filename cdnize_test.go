package main

import (
	"testing"
)

func TestRandString(t *testing.T) {
	rv := randString(5)
	testStringNeq(t, "test RandString Not equal `abc`...", rv, "abc")
	if len(rv) != 5 {
		t.Errorf("Len should be 5 got `%d`", len(rv))
	}
	rv2 := randString(6)
	testStringNeq(t, "test RandString not equal `"+rv2+"`", rv, rv2)
}

func testStringEq(t *testing.T, msg, actual, expected string) {
	if actual != expected {
		t.Errorf("%s: `%s` != `%s`", msg, actual, expected)
	}
}

func testStringNeq(t *testing.T, msg, actual, notExpected string) {
	if actual == not_expected {
		t.Errorf("%s: `%s` == `%s`", msg, actual, notExpected)
	}
}
