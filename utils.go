/**
*
* 	AnsCDN Copyright (C) 2010 Robin Syihab (r [at] nosql.asia)
*	Simple CDN server written in Golang.
*
*	License: General Public License v2 (GPLv2)
*
*	Copyright (c) 2009 The Go Authors. All rights reserved.
*
**/

package main

import (
	"encoding/json"
	"math/rand"
	"os"
	"syscall"
	"time"
	"unicode/utf8"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func fixedMime(mimeType string) string {
	fixedMimeList := map[string]string{
		"js": "application/x-javascript",
	}

	variantMimeList := map[string]string{
		"application/javascript": fixedMimeList["js"],
	}

	if v, ok := variantMimeList[mimeType]; ok {
		return v
	}
	return mimeType
}

func isFileExists(path string) bool {
	_, err := os.Stat(path)
	if err != nil {
		return false
	}
	return true
}

func randString(n int) string {
	chars := "ABCDEFGHIJKLMNOPQRSTUVWXYZ123567890abcdefghijklmnopqrstuvwxyz_"
	charsLen := len(chars)

	buf := make([]byte, n)

	for i := 0; i < n; i++ {
		buf[i] = chars[rand.Intn(charsLen)]
	}
	return string(buf)
}

func modTimeString(file *os.File) (string, error) {
	st, err := file.Stat()
	if err != nil {
		return "", err
	}

	return st.ModTime().Format(time.RFC1123), nil
}

// TODO: getAtime currently Linux depedent! fix it!!
func getAtime(path string) (time.Time, error) {
	fi, err := os.Stat(path)
	if err != nil {
		return time.Now(), err
	}
	stat := fi.Sys().(*syscall.Stat_t)
	return time.Unix(int64(stat.Atim.Sec), int64(stat.Atim.Nsec)), nil
}

func jsonize(data interface{}) string {
	rv, err := json.Marshal(&data)
	if err != nil {
		logError("Cannot jsonize `%v`\n", data)
		return ""
	}
	return string(rv)
}

// Heuristic: b is text if it is valid UTF-8 and doesn't
// contain any unprintable ASCII or Unicode characters.
func isText(b []byte) bool {
	for len(b) > 0 && utf8.FullRune(b) {
		rune, size := utf8.DecodeRune(b)
		if size == 1 && rune == utf8.RuneError {
			// decoding error
			return false
		}
		if 0x80 <= rune && rune <= 0x9F {
			return false
		}
		if rune < ' ' {
			switch rune {
			case '\n', '\r', '\t':
				// okay
			default:
				// binary garbage
				return false
			}
		}
		b = b[size:]
	}
	return true
}
