/**
*
* 	AnsCDN Copyright (C) 2010 Robin Syihab (r [at] nosql.asia)
*	Simple CDN server written in Golang.
*
*	License: General Public License v2 (GPLv2)
*
*	Copyright (c) 2009 The Go Authors. All rights reserved.
*
**/

package main

import (
	"os"
	"path/filepath"
	"strings"
	"time"
)

func rmIfObsolete(fpath string, cacheExpires time.Duration) error {
	atime, err := getAtime(fpath)
	if err != nil {
		logError("%s", err.Error())
		return err
	}

	old := time.Since(atime)
	if old > cacheExpires {
		logInfo("File `%s` is obsolete, %s old.\n", fpath, old)
		logInfo("Delete file `%s`\n", fpath)
		if err := os.Remove(fpath); err != nil {
			logError("Cannot delete file `%s`. e: %s\n", err.Error())
			return err
		}
	}

	return nil
}

func processDir(p string, cacheExpires time.Duration) error {
	err := filepath.Walk(p, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		_, fn := filepath.Split(path)
		if strings.HasPrefix(fn, ".") {
			if info.IsDir() {
				return filepath.SkipDir
			}
			return nil
		}

		return rmIfObsolete(path, cacheExpires)
	})

	return err
}

func startFileMon(storeDir string, cx int64) {
	logInfo("File monitor started. (`%s`)\n", storeDir)

	for {
		//logInfo("Starting file auto cleaner...\n")
		if err := processDir(storeDir, time.Duration(cx)*time.Second); err != nil {
			logError("failed in precessDir: %s", err.Error())
		}

		time.Sleep(24 * time.Hour)
	}
}
