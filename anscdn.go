/**
*
* 	AnsCDN Copyright (C) 2010 Robin Syihab (r [at] nosql.asia)
*	Simple CDN server written in Golang.
*
*	License: General Public License v2 (GPLv2)
*
*	Copyright (c) 2009 The Go Authors. All rights reserved.
*
**/

package main

import (
	"flag"
	"fmt"
	"io"
	"mime"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
)

// VERSION text
const VERSION = "0.14"

var cfg *AnscdnConf

func setHeaderCond(con http.ResponseWriter, absPath string, data []byte) {
	extension := path.Ext(absPath)
	if ctype := mime.TypeByExtension(extension); ctype != "" {
		con.Header().Set("Content-Type", ctype)
	} else {
		if isText(data) {
			con.Header().Set("Content-Type", "text-plain; charset=utf-8")
		} else {
			con.Header().Set("Content-Type", "application/octet-stream") // generic binary
		}
	}
}

func validURLPath(urlPath string) bool {
	return strings.Index(urlPath, "../") < 1
}

func mainHandler(con http.ResponseWriter, r *http.Request) {

	urlPath := r.URL.Path[1:]

	if len(urlPath) == 0 {
		http.Error(con, "404", http.StatusNotFound)
		return
	}

	// security check
	if !validURLPath(urlPath) {
		fmt.Fprintf(con, "Invalid url path")
		logWarn("Invalid urlPath: %s\n", urlPath)
		return
	}

	if len(cfg.URLMap) > 1 && strings.HasPrefix(urlPath, cfg.URLMap[1:]) == true {
		urlPath = urlPath[len(cfg.URLMap):]
	}

	// restrict no ext
	if cfg.IgnoreNoExt && len(path.Ext(urlPath)) == 0 {
		logWarn("Ignoring `%s`\n", urlPath)
		http.Error(con, "404", http.StatusNotFound)
		return
	}

	// restrict ext
	if len(cfg.IgnoreExt) > 0 {
		cext := path.Ext(urlPath)
		if len(cext) > 1 {
			cext = strings.ToLower(cext[1:])
			exts := strings.Split(cfg.IgnoreExt, ",")
			for _, ext := range exts {
				if cext == strings.Trim(ext, " ") {
					logWarn("Ignoring `%s` by extension.\n", urlPath)
					http.Error(con, "404", http.StatusNotFound)
					return
				}
			}
		}
	}

	var absPath string

	if strings.HasPrefix(cfg.StoreDir, "./") {
		absPath, _ = os.Getwd()
		absPath = path.Join(absPath, cfg.StoreDir[1:], urlPath)
	} else {
		absPath = path.Join(cfg.StoreDir, urlPath)
	}

	dirName, _ := path.Split(absPath)

	if !isFileExists(absPath) {

		urlSource := "http://" + cfg.BaseServer + "/" + urlPath

		err := os.MkdirAll(dirName, 0755)
		if err != nil {
			fmt.Fprintf(con, "404 Not found (e)")
			logError("Cannot MkdirAll. error: %s\n", err.Error())
			return
		}

		// download it
		var data []byte
		rv, lm, totalSize := download(urlSource, absPath, cfg.Strict, &data)
		if rv == false {
			fmt.Fprintf(con, "404 Not found")
			return
		}

		// send to client for the first time.
		setHeaderCond(con, absPath, data)

		// set Last-modified header
		con.Header().Set("Last-Modified", lm)

		for {
			bw, err := con.Write(data)
			if err != nil || bw == 0 {
				break
			}
			if bw >= totalSize {
				break
			}
		}

	} else {

		if cfg.CacheOnly {
			// no static serving, use external server like nginx etc.
			return
		}

		// if file exists, just send it
		file, err := os.Open(absPath)
		if err != nil {
			fmt.Fprintf(con, "404 Not found (e)")
			logError("Cannot open file `%s`. error: %s\n", absPath, err.Error())
			return
		}

		defer file.Close()

		bufsize := 1024 * 4
		buff := make([]byte, bufsize+2)

		sz, err := file.Read(buff)
		if err != nil && err != io.EOF {
			fmt.Fprintf(con, "404 Not found (e)")
			logError("Cannot read %d bytes data in file `%s`. error: %s\n", sz, absPath, err.Error())
			return
		}

		setHeaderCond(con, absPath, buff)

		// check for last-modified
		//r.Header["If-Modified-Since"]
		lm, _ := modTimeString(file)
		con.Header().Set("Last-Modified", lm)

		if r.Header.Get("If-Modified-Since") == lm {
			con.WriteHeader(http.StatusNotModified)
			return
		}

		con.Write(buff[0:sz])

		for {
			sz, err := file.Read(buff)
			if err != nil {
				if err == io.EOF {
					con.Write(buff[0:sz])
					break
				}
				fmt.Fprintf(con, "404 Not found (e)")
				logError("Cannot read %d bytes data in file `%s`. error: %s\n", sz, absPath, err.Error())
				return
			}
			con.Write(buff[0:sz])
		}

	}

}

func clearCacheHandler(c http.ResponseWriter, r *http.Request) {

	pathToClear := r.FormValue("p")
	if len(pathToClear) == 0 {
		fmt.Fprintf(c, "Invalid parameter")
		return
	}

	// prevent canonical path
	if strings.HasPrefix(pathToClear, ".") {
		fmt.Fprintf(c, "Bad path")
		return
	}
	if pathToClear[0] == '/' {
		pathToClear = pathToClear[1:]
	}
	pathToClear = "./data/" + pathToClear

	f, err := os.Open(pathToClear)
	if err != nil {
		logError("File open error %s\n", err.Error())
		fmt.Fprintf(c, "Invalid request")
		return
	}
	defer f.Close()

	st, err := f.Stat()
	if err != nil {
		logError("Cannot stat file. error %s\n", err.Error())
		fmt.Fprintf(c, "Invalid request")
		return
	}
	if !st.IsDir() {
		fmt.Fprintf(c, "Invalid path")
		return
	}

	err = os.RemoveAll(pathToClear)
	if err != nil {
		fmt.Fprintf(c, "Cannot clear path. e: %s", err.Error())
		return
	}

	storeDir := cfg.StoreDir

	if pathToClear == storeDir+"/" {
		if err := os.Mkdir(storeDir, 0775); err != nil {
			logError("Cannot recreate base storeDir: `%s`\n", storeDir)
		}
	}

	logInfo("Path cleared by request from `%s`: `%s`\n", r.Host, pathToClear)
	fmt.Fprintf(c, "Clear successfully")
}

func intro() {
	fmt.Println("")
	fmt.Println(" AnsCDN " + VERSION + " - a Simple CDN Server")
	fmt.Println(" Copyright (C) 2010 Robin Syihab (r@nosql.asia)")
	fmt.Println(" Under GPLv2 License")
	fmt.Println(" AnsCDN " + VERSION + " - a Simple CDN Server")
	fmt.Println("")
}

func main() {

	intro()

	var cfgFile string

	flag.StringVar(&cfgFile, "config", "anscdn.cfg", "Config file.")
	flag.BoolVar(&logQuiet, "quiet", false, "Quiet.")

	flag.Parse()

	var err error
	cfg, err = configParse(cfgFile)
	if err != nil {
		fmt.Println("Invalid configuration:", err)
		os.Exit(1)
	}

	fmt.Println("Configuration:")
	fmt.Println("---------------------------------------")

	fmt.Println("Base server: " + cfg.BaseServer)
	if cfg.Strict == true {
		fmt.Println("Strict mode ON")
	} else {
		fmt.Println("Strict mode OFF")
	}
	if cfg.CacheOnly == true {
		fmt.Println("Cache only")
	}
	if cfg.IgnoreNoExt == true {
		fmt.Println("Ignore no extension files")
	}
	if len(cfg.IgnoreExt) > 0 {
		fmt.Println("Ignore extension for", cfg.IgnoreExt)
	}
	if len(cfg.ClearCachePath) > 0 {
		fmt.Println("Clear cache path: ", cfg.ClearCachePath)
	}

	fmt.Printf("Store cached data in `%s`\n", cfg.StoreDir)

	if cfg.FileMon == true {
		fmt.Println("File monitor enabled")
		if err != nil {
			logError("Invalid cache_expires value `%d`\n", cfg.CacheExpires)
			os.Exit(5)
		}
		go startFileMon(cfg.StoreDir, cfg.CacheExpires)
	}

	currDir, _ := path.Split(os.Args[0])
	os.Chdir(currDir)
	currDir, err = os.Getwd()
	if err != nil {
		logError("Cannot get currDirectory\n")
		os.Exit(6)
	}
	logInfo("Current directory: %v\n", currDir)
	fi, err := os.Lstat(currDir + cfg.StoreDir[1:])
	if err != nil || fi.IsDir() == false {
		err = os.Mkdir(currDir+cfg.StoreDir[1:], 0755)
		if err != nil {
			logError("Cannot create dir `%s`. %s.\n", err)
			os.Exit(8)
		}
	}

	fmt.Println("---------------------------------------")

	logInfo("Serving on 0.0.0.0:%d... ready.\n", cfg.ServingPort)

	if len(cfg.ClearCachePath) > 0 {
		if cfg.ClearCachePath[0] != '/' {
			logError("Invalid ccp `%s`. missing `/`\n", cfg.ClearCachePath)
			os.Exit(2)
		}
		http.HandleFunc(cfg.ClearCachePath, clearCacheHandler)
	}
	if cfg.ProvideAPI == true {
		http.HandleFunc("/api/cdnize", cdnHandler)

		fi, err := os.Lstat(currDir + cfg.StoreDir[1:] + "/" + cfg.APIStorePrefix)
		if err != nil || fi.IsDir() == false {
			err = os.Mkdir(currDir+cfg.StoreDir[1:]+"/"+cfg.APIStorePrefix, 0755)
			if err != nil {
				logError("Cannot create dir `%s`. %s.\n", err)
				os.Exit(8)
			}
		}

		http.HandleFunc(fmt.Sprintf("/%s/", cfg.StoreDir[2:]), cdnStaticHandler)
	}
	http.HandleFunc("/", mainHandler)
	if err := http.ListenAndServe("0.0.0.0:"+strconv.Itoa(cfg.ServingPort), nil); err != nil {
		logError("%s\n", err.Error())
	}
}
